﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Challenge_3
{
    namespace Spaceship
    {
        [RequireComponent(typeof(SpaceshipController))]
        public class SpaceshipBounds : MonoBehaviour
        {
            [SerializeField] private Collider _screen;

            [SerializeField] public float xMax;
            [SerializeField] public float xMin;
            [SerializeField] public float yMax;
            [SerializeField] public float yMin;
            
            private void Start()
            {
                xMax = _screen.bounds.max.x;
                xMin = _screen.bounds.min.x;
                yMax = _screen.bounds.max.y;
                yMin = _screen.bounds.min.y;
            }
            
            public Vector3 ValidatePosition(Vector3 position)
            {
                Vector3 newPos = position;

                if (newPos.x > xMax)
                {
                    newPos.x = xMin;
                }

                if(newPos.x < xMin)
                {
                    newPos.x = xMax;
                }

                if (newPos.y > yMax)
                {
                    newPos.y = yMin;
                }

                if (newPos.y < yMin)
                {
                    newPos.y = yMax;
                }

                return newPos;
            }

        }
    }
}