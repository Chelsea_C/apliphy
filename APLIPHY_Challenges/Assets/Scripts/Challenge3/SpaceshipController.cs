﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Challenge_3
{
    namespace Spaceship
    {
        [RequireComponent(typeof(SpaceshipBounds))]
        public class SpaceshipController : MonoBehaviour
        {
            private Vector3 _position = new Vector3();
            private Vector3 _velocity = new Vector3();
            private Vector3 _acceleration = new Vector3();
            private Vector3 _currentVRotation = new Vector3();
            private Quaternion _currentQRotation;

            [Header("Physics")]
            [Tooltip("The amount of change in angle for a step in rotation. Can be used as rotation speed.")]
            [SerializeField] private float _angleStep;
            [SerializeField] private float _topSpeed = 1f;
            [SerializeField] private Vector3 _force;

            [Header("Controls")]
            [Tooltip("Apply force to accelerate the spaceship in its current direction.")]
            [SerializeField] private KeyCode _boost = KeyCode.Z;
            [SerializeField] private KeyCode _boostAlternate = KeyCode.Space;

            [SerializeField] private KeyCode _rotateLeft = KeyCode.A;
            [SerializeField] private KeyCode _rotateRight = KeyCode.D;



            private float _currentAngle;
            
            private SpaceshipBounds _bounds;


            private void Start()
            {
                _currentQRotation = transform.rotation;
                _position = transform.position;
                _velocity = Vector3.zero;
                _acceleration = Vector3.zero;
                _currentAngle = 0f;

                _bounds = GetComponent<SpaceshipBounds>();
                
            }

            private void Update()
            {
                if (Input.GetKey(_rotateLeft))
                {
                    SteerLeft();
                }
                else if (Input.GetKey(_rotateRight))
                {
                    SteerRight();
                }

                if (Input.GetKey(_boost))
                {
                    ApplyForce();
                }
                else
                {
                    Decelerate();
                }

                _currentQRotation.eulerAngles = _currentVRotation;
                transform.rotation = _currentQRotation;
                
                Move();
            }

            private void Move()
            {
                if (_velocity.x + _acceleration.x > _topSpeed)
                    _velocity.x = _topSpeed;
                else
                    _velocity += _acceleration;

                _position += _velocity;

                // Check if at edge
                _position = _bounds.ValidatePosition(_position);

                // Move to new position
                transform.position = _position;

                // To eventually come to a stop
                _velocity *= 0.99f;
            }
            
            public void SteerLeft()
            {
                _currentVRotation.z -= _angleStep;
                _currentAngle -= _angleStep;
            }


            public void SteerRight()
            {
                _currentVRotation.z += _angleStep;
                _currentAngle += _angleStep;
            }


            public void ApplyForce()
            {
                Vector3 rotatedForce = Quaternion.AngleAxis(_currentAngle, Vector3.forward) * _force;
                _acceleration = rotatedForce;
                _velocity += _acceleration;
            }


            public void Decelerate()
            {
                _acceleration = Vector3.zero;
                _velocity += _acceleration;
            }
        }
    }
}