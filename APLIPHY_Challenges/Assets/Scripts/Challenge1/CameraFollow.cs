﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Challenge_1
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Vector3 offset;
        [SerializeField] private GameObject target;

        void Start()
        {
            offset = transform.position - target.transform.position;
        }

        void LateUpdate()
        {
            transform.position = target.transform.position + offset;
        }
    }
}