﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Challenge_1
{
    namespace Car
    {
        public enum STATE
        {
            NULL,
            ACCELERATING,
            MAXSPEED,
            DECELERATING,
            REVERSING,
            IDLE
        }

        public enum DIR
        {
            NULL,
            LEFT,
            RIGHT
        }

        [RequireComponent(typeof(CarControls))]
        public class CarMovement : MonoBehaviour
        {
            [SerializeField] private Vector3 _velocity = new Vector3();

            [SerializeField] private float _accelerationFactor = 1f;
            [SerializeField] private float _decelerationFactor = 1.5f;
            [SerializeField] private float _reverseFactor = 2f;

            [SerializeField] private float _topSpeed = 10f;
            [SerializeField] private float _rotationSpeed = 30f;
            [SerializeField] private STATE _state;

            private Vector3 _position = new Vector3();
            private Vector3 _acceleration = new Vector3();

            private Quaternion _currentRotation;
            private Vector3 _currentEulerAngles = new Vector3();

            private void Start()
            {
                _position = transform.position;
                _currentRotation = transform.rotation;
            }

            private void Update()
            {
                Move();
                Debug.Log(_acceleration.x);
            }

            private void Move()
            {
                if (_velocity.x + _acceleration.x > _topSpeed)
                    _velocity.x = _topSpeed;
                else
                    _velocity += _acceleration;

                if (_velocity.x < 0)
                    _velocity = Vector3.zero;

                
                _position += transform.rotation * _velocity;
                transform.position = _position;
            }

            public void Accelerate()
            {
                if (_velocity.x <= _topSpeed)
                    _state = STATE.MAXSPEED;
                else
                    _state = STATE.ACCELERATING;
                _acceleration.x = _accelerationFactor;
            }

            public void Reverse()
            {
                _state = STATE.REVERSING;
                _acceleration.x = -1 * _reverseFactor;
            }

            public void Decelerate()
            {
                if (_velocity.x <= 0)
                    _state = STATE.IDLE;
                else
                    _state = STATE.DECELERATING;

                _acceleration.x = -1 * _decelerationFactor;
            }

            public void Steer(DIR nSteerDirection)
            {
                switch (nSteerDirection)
                {
                    case DIR.LEFT:
                        _currentEulerAngles += new Vector3(0, -1, 0) * _rotationSpeed * Time.deltaTime;
                        break;

                    case DIR.RIGHT:
                        _currentEulerAngles += new Vector3(0, 1, 0) * _rotationSpeed * Time.deltaTime;
                        break;

                    default:
                        break;
                }

                _currentRotation.eulerAngles = _currentEulerAngles;
                transform.rotation = _currentRotation;
            }

        }
    }
}