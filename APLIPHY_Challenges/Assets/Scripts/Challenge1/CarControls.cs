﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Challenge_1
{
    namespace Car
    {
        [RequireComponent(typeof(CarMovement))]
        public class CarControls : MonoBehaviour
        {
            [SerializeField] private KeyCode _forward;
            [SerializeField] private KeyCode _backward;

            [SerializeField] private KeyCode _left;
            [SerializeField] private KeyCode _right;

            private CarMovement car;

            private void Start()
            {
                car = GetComponent<CarMovement>();
            }

            private void Update()
            {
                if (Input.GetKey(_forward) || Input.GetKey("up"))
                {
                    if (Input.GetKey(_left) || Input.GetKey("left"))
                    {
                        car.Steer(DIR.LEFT);
                    }

                    if (Input.GetKey(_right) || Input.GetKey("right"))
                    {
                        car.Steer(DIR.RIGHT);
                    }

                    car.Accelerate();
                }
                else if (Input.GetKey(_backward) || Input.GetKey("down"))
                {
                    car.Reverse();
                }
                else
                {
                    car.Decelerate();
                }

            }

        }
    }
}