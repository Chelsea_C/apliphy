﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class BirdPath : MonoBehaviour
{
    [SerializeField] private Bird _bird;
    [SerializeField] private float _maxTime = 100f;
    [SerializeField] private int _maxMarks = 30;


    private LineRenderer _lineRenderer;
    private Bird _path;

    private void Start()
    {
        transform.position = _bird.transform.position;
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.startWidth = 1;
        _lineRenderer.endWidth = 1;
    }


    void Update()
    {
        //Vector3 _velocity = _bird.launchVector;

        //_lineRenderer.positionCount = ((int)(_maxTime / _bird.timeResolution));

        //int index = 0;
        //Vector3 currentPosition = transform.position;

        //for (float t = 0.0f; t < _maxTime; t += _bird.timeResolution)
        //{
        //    _lineRenderer.SetPosition(index, currentPosition);
        //    currentPosition += _velocity;
        //    _velocity += _bird.gravity / _bird.mass * _bird.timeResolution;
        //    index++;
        //}

        Vector3 currentPosition = transform.position;

        //_lineRenderer.positionCount = ((int)(_maxTime / _bird.timeResolution));
        _lineRenderer.positionCount = _maxMarks;

        int index = 0;

        //for (float t = 0.0f; t < _maxTime; t += _bird.timeResolution)
        //{
        //    _lineRenderer.SetPosition(index, currentPosition);
        //    currentPosition.x = _bird.launchForce * t * Mathf.Cos(_bird.angle);
        //    currentPosition.y = _bird.launchForce * t * Mathf.Sin(_bird.angle) - 0.5f * Mathf.Abs(_bird.gravity.y) * t * t;
        //    index++;
        //}

        for (int i = 0; i < _maxMarks; i ++)
        {
            _lineRenderer.SetPosition(index, currentPosition);
            currentPosition.x += _bird.launchVector.x * i * Mathf.Cos(_bird.angle);
            currentPosition.y += _bird.launchVector.y * i * Mathf.Sin(_bird.angle) - 0.5f * -9.81f * i * i;
            index++;
        }

    }
}
     
