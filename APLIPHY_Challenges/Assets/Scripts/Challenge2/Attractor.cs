﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour
{
    [SerializeField] private float _mass;
    [SerializeField] private float _radius;
    [SerializeField] private Vector3 _gravity = new Vector3(0f, -9.81f, 0f);

    [SerializeField] private Bird _bird;

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }

    public Vector3 CalculateAttraction(Bird bird)
    {
        float distance = Vector3.Distance(_bird.transform.position, transform.position);
        return _gravity * (bird.mass * _mass) / (distance * distance);
    }
}


/*
 F = G(m1 * m2) / d^2

    d = m1.transform.position - m2.transform.position   
     G = (0,-9.81, 0)
     m1 = m1.mass
     m2 = m2.mass   
     Force=
     
     
   
     */