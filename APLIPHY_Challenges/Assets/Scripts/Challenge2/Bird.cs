﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    [Header("Set")]
    
    [SerializeField] private float _mass = 1.0f;
    [SerializeField] private float _radius = 1.0f;
    [SerializeField] private float _maxLaunchForce = 20.0f;
    [SerializeField] private float _timeResolution = 0.02f;

    [SerializeField] private Vector3 _initialPosition;
    [SerializeField] private Vector3 _gravity = new Vector3(0f, -9.81f, 0f);
    
    [SerializeField] private GameObject _markersParent;
    [SerializeField] GameObject[] _trailMarkers;

    [Header("Update")]
    [SerializeField] private  bool _isLaunched;
    [SerializeField] private bool _isDragging;
    [SerializeField] private float _deltaT;
    [SerializeField] private float _launchForce = 10.0f;
    [SerializeField] private Vector3 _launchPosition;
    [SerializeField] private Vector3 _launchVector;
    [SerializeField] private Vector3 _velocity;
    [SerializeField] private Vector3 _acceleration;

    public float angle;

    #region Getters/Setters
    public float mass { get { return _mass; }  }
    public float radius { get { return _radius; }  }
    public float timeResolution { get { return _timeResolution; } }
    public Vector3 gravity { get { return _gravity; } }
    public float launchForce { get { return _launchForce; } }
    public float maxLaunchForce { get { return _maxLaunchForce; } }
    public Vector3 launchVector { get { return _launchVector; } set { _launchVector = value; } }
    public bool IsDragging { get { return _isDragging; } }
    public bool IsLaunched { get { return _isLaunched; }  }
    #endregion
    

    void Start()
    {
        _initialPosition = transform.position;
        _launchPosition = transform.position;

        _acceleration = Vector3.zero;
        _velocity = Vector3.zero;

        _isLaunched = false;
        _isDragging = false;

        DisableTrailMarkers();
        
    }
    
    void Update()
    {
        
        if (_isLaunched)
        {
            _deltaT += _timeResolution;

            _velocity = _launchVector;
            _acceleration += _gravity / _mass;
            _velocity += _acceleration * _deltaT;

            transform.position += _velocity;

            _acceleration *= 0;

            DrawTrail();

        }
        else
            _deltaT = 0;

    }

    private void OnMouseUp()
    {
        if (!_isDragging)
            return;

        Launch();
        _isDragging = false;
    }

    private void OnMouseDrag()
    {
        if (_isLaunched)
            return;

        _isDragging = true;

        // Get position to launch from
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector3 offset = new Vector3(mousePos.x, mousePos.y, 0) - _initialPosition;
        transform.position = _initialPosition + Vector3.ClampMagnitude(offset, _maxLaunchForce);

        _launchPosition = transform.position;

        // Get direction of launch
        _launchVector = _initialPosition - _launchPosition;

        // Clamp vector magnitude to max limit 
        _launchVector = Vector3.ClampMagnitude(_launchVector, maxLaunchForce);
        _launchForce = _launchVector.magnitude;

        angle = Mathf.Atan2(_launchVector.normalized.y, _launchVector.normalized.x) * Mathf.Rad2Deg;
    }

    public void Launch()
    {
        if (_isLaunched)
            return;
        _isLaunched = true;
    }
    
    private void DrawTrail()
    {
        if (!_isLaunched)
            return;

        GameObject marker = Instantiate(_trailMarkers[0], transform.position, Quaternion.identity, _markersParent.transform);
        marker.SetActive(true);

        for(int i = 0; i < _trailMarkers.Length; i++)
        {
            //place evenly
        }

    }
    
    private void DisableTrailMarkers()
    {
        for (int i = 0; i < _trailMarkers.Length; i++)
            _trailMarkers[i].gameObject.SetActive(false);
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(_initialPosition, _launchPosition);
    }

}
